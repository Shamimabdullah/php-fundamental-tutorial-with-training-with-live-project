<?php

echo "<h3>Indexed array </h3><br>";

$shamim = array('Noman', 'Shafak', 'Tumpa','Ahsan');

print_r($shamim);
echo '<br>';
echo $shamim[1];

echo '<hr>';
echo "<h3>Associative array </h3><br>";

$shamim = array('Noman'=>25, 'Shafak'=>15, 'Tumpa'=>30,'Ahsan'=>27);
print_r($shamim);
echo '<br>';
echo $shamim['Shafak'];

echo '<hr>';


echo "<h4>Multidimensional Array </h4><br>";

$mother = array(

    'shamim'=> array('age'=>27, 'sub'=> 'CSE', 'hobby'=> 'Coding'),
    'noman'=> array('age'=> 24, 'sub'=> 'Criminology', 'hobby'=> 'Reporting'),
    'jebin'=> array('age'=>25, 'sub'=> 'Sociology', 'hobby'=> 'Reading'),
    'shafak'=>array('age'=> 15, 'sub'=> 'General', 'hobby'=> 'Playing Cricket')


);

print_r($mother);
echo '<br>';
echo '<hr>';

echo $mother['noman']['hobby'];


?>